import { DbModule } from '@neb-sports/mysamay-db-provider';
import { Test, TestingModule } from '@nestjs/testing';



import { RunningGroupRepository } from "./running-group-repository";


describe('RunningGroupRepository', () => {
    let provider: RunningGroupRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [DbModule],
            providers: [RunningGroupRepository],
        }).compile();

        provider = module.get<RunningGroupRepository>(RunningGroupRepository);
    });

    it('should be defined', () => {
        expect(provider).toBeDefined();
    });

});
