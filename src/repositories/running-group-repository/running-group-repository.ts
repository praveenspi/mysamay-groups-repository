import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember, SortOptionObject } from 'mongodb';

import { MongoProvider } from '@neb-sports/mysamay-db-provider';
import { RunningGroup } from "@neb-sports/mysamay-groups-model";

@Injectable()
export class RunningGroupRepository {
    db: Db;
    collection: Collection<RunningGroup>;

    constructor(private dbProvider: MongoProvider) { }

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(RunningGroup.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createGroup(group: RunningGroup): Promise<RunningGroup> {
        await this.getCollection();
        let result = await this.collection.insertOne(group);
        if (result.insertedCount === 1) return result.ops[0];
        return undefined;
    }

    async updateGroup(group: RunningGroup): Promise<RunningGroup> {
        await this.getCollection();
        let result = await this.collection.findOneAndReplace(
            { _id: group._id },
            group,
            { returnOriginal: false },
        );
        if (result.ok === 1) return result.value;
        else return undefined;
    }

    async getGroupByAnyQuery(
        query: FilterQuery<RunningGroup>,
        projection: SchemaMember<RunningGroup, any>,
    ): Promise<RunningGroup> {
        try {
            await this.getCollection();
            let result = await this.collection.findOne(query, {
                projection: projection,
                collation: {
                    locale: "en"
                }
            });
            if (result) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async countGroupByAnyQuery(
        query: FilterQuery<RunningGroup>,
    ): Promise<number> {
        await this.getCollection();
        return await this.collection.countDocuments(query);
    }

    async getGroupByAnyQueryPaginated(
        query: FilterQuery<RunningGroup>,
        skip: number,
        limit: number,
        sort: SortOptionObject<RunningGroup>
    ): Promise<RunningGroup[]> {
        await this.getCollection();
        let result = await this.collection
        .find(query)
        .collation({ locale: 'en' })
        .sort(sort)
        .skip(skip)
        .limit(limit)
        .toArray();
        if (result) return result;
        else return undefined;
    }

    async deleteGroupByAnyQuery(query: FilterQuery<RunningGroup>) {
        await this.getCollection();
        await this.collection.deleteMany(query);
    }

    async bulkInsert(groups: RunningGroup[]) {
        await this.getCollection();
        let bulk = this.collection.initializeUnorderedBulkOp();
        for (let group of groups) {
            bulk.insert(group);
        }
        await bulk.execute();
    }

    async searchRunningGroups(queryStr: string,
        projection: SchemaMember<RunningGroup, any>): Promise<RunningGroup[]> {
        try {
            await this.getCollection();
            let result = await this.collection.find({
                groupName: { $regex: ".*" + queryStr + ".*", $options: "i" }
            }, { projection: projection })
                .skip(0)
                .limit(10)
                .toArray();
            if (result) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }
}
