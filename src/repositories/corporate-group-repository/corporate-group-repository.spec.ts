import { DbModule } from '@neb-sports/mysamay-db-provider';
import { Test, TestingModule } from '@nestjs/testing';



import { CorporateGroupRepository } from "./corporate-group-repository";


describe('CorporateGroupRepository', () => {
    let provider: CorporateGroupRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [DbModule],
            providers: [CorporateGroupRepository],
        }).compile();

        provider = module.get<CorporateGroupRepository>(CorporateGroupRepository);
    });

    it('should be defined', () => {
        expect(provider).toBeDefined();
    });

});
