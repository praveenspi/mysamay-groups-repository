import { Module } from '@nestjs/common';
import { DbModule } from '@neb-sports/mysamay-db-provider';



import { RunningGroupRepository } from '../repositories/running-group-repository/running-group-repository';
import { CorporateGroupRepository } from '../repositories/corporate-group-repository/corporate-group-repository';


@Module({
    imports: [DbModule],
    providers: [RunningGroupRepository, CorporateGroupRepository],
    exports: [RunningGroupRepository, CorporateGroupRepository],
})
export class GroupsRepositoryModule {}
